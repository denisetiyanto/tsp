/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50518
Source Host           : localhost:3306
Source Database       : online_test

Target Server Type    : MYSQL
Target Server Version : 50518
File Encoding         : 65001

Date: 2016-06-30 21:15:26
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `antrian`
-- ----------------------------
DROP TABLE IF EXISTS `antrian`;
CREATE TABLE `antrian` (
  `id_antrian` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keluhan` varchar(255) NOT NULL,
  `level_progress` tinyint(1) NOT NULL DEFAULT '1',
  `tgl_perbaikan` timestamp NULL DEFAULT NULL,
  `teknisi` int(11) DEFAULT NULL,
  `tgl_selesai` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_antrian`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian
-- ----------------------------
INSERT INTO `antrian` VALUES ('2', '1', '2016-06-30 02:27:41', 'mati', '3', '2016-06-30 01:17:00', '1', '2016-06-30 08:57:13');
INSERT INTO `antrian` VALUES ('3', '1', '2016-06-30 14:01:03', 'Kedap kedip', '1', null, null, null);

-- ----------------------------
-- Table structure for `divisi`
-- ----------------------------
DROP TABLE IF EXISTS `divisi`;
CREATE TABLE `divisi` (
  `id_divisi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id_divisi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of divisi
-- ----------------------------
INSERT INTO `divisi` VALUES ('1', 'Penjualan');
INSERT INTO `divisi` VALUES ('2', 'IT');
INSERT INTO `divisi` VALUES ('3', 'Gudang');

-- ----------------------------
-- Table structure for `inventaris`
-- ----------------------------
DROP TABLE IF EXISTS `inventaris`;
CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_divisi` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `kondisi` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inventaris
-- ----------------------------
INSERT INTO `inventaris` VALUES ('1', 'K001', '1', '1', null, 'Komputer', 'Baek', '1');
INSERT INTO `inventaris` VALUES ('2', 'P001', '2', '1', null, 'Canon xxx', 'Baek', '0');
INSERT INTO `inventaris` VALUES ('3', 'M001', '3', '1', null, 'LG ', 'Baek', '0');
INSERT INTO `inventaris` VALUES ('4', 'K002', '1', '3', null, 'Komputer', 'Baek', '0');
INSERT INTO `inventaris` VALUES ('5', 'B001', '4', '3', null, 'Honeywell', 'Baek', '0');

-- ----------------------------
-- Table structure for `karyawan`
-- ----------------------------
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `id_divisi` int(11) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
INSERT INTO `karyawan` VALUES ('1', 'Marwoto', '2', 'Teknisi');
INSERT INTO `karyawan` VALUES ('2', 'Joko', '2', 'Manager');
INSERT INTO `karyawan` VALUES ('3', 'Kevin', '2', 'Admin IT');
INSERT INTO `karyawan` VALUES ('4', 'Toni', '3', 'Admin Gudang');
INSERT INTO `karyawan` VALUES ('5', 'Purwoko', '1', 'Admin Penjualan');
INSERT INTO `karyawan` VALUES ('6', 'Iwan', '2', 'Teknisi');

-- ----------------------------
-- Table structure for `kategori`
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES ('1', 'Komputer');
INSERT INTO `kategori` VALUES ('2', 'Printer');
INSERT INTO `kategori` VALUES ('3', 'Monitor');
INSERT INTO `kategori` VALUES ('4', 'Barcode Reader');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin_it', '5788b6f3ebbfc912f1cc936e41edabe2e688f465', '3');
INSERT INTO `user` VALUES ('2', 'manager_it', '778d511b1b6751138bdc8545cdfd22d5f201bac0', '2');
INSERT INTO `user` VALUES ('3', 'gudang', 'a80dd043eb5b682b4148b9fc2b0feabb2c606fff', '4');
INSERT INTO `user` VALUES ('4', 'penjualan', 'cbaf910575ba6349081e729b363ee88cf1f1057d', '5');

-- ----------------------------
-- View structure for `v_antrian`
-- ----------------------------
DROP VIEW IF EXISTS `v_antrian`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_antrian` AS select `karyawan`.`id_karyawan` AS `id_karyawan`,`karyawan`.`nama` AS `karyawan_nama`,`karyawan`.`jabatan` AS `jabatan`,`v_inventaris`.`id_inventaris` AS `id_inventaris`,`v_inventaris`.`kode` AS `kode`,`v_inventaris`.`inventaris_id_kategori` AS `inventaris_id_kategori`,`v_inventaris`.`inventaris_id_divisi` AS `inventaris_id_divisi`,`v_inventaris`.`keterangan` AS `keterangan`,`v_inventaris`.`nama` AS `nama`,`v_inventaris`.`kondisi` AS `kondisi`,`v_inventaris`.`id_kategori` AS `id_kategori`,`v_inventaris`.`kategori_nama` AS `kategori_nama`,`v_inventaris`.`id_divisi` AS `id_divisi`,`v_inventaris`.`divisi_nama` AS `divisi_nama`,`antrian`.`id_antrian` AS `id_antrian`,`antrian`.`id_inventaris` AS `antrian_id_inventaris`,`antrian`.`tgl` AS `tgl`,`antrian`.`keluhan` AS `keluhan`,`antrian`.`level_progress` AS `level_progress`,`antrian`.`tgl_perbaikan` AS `tgl_perbaikan`,`antrian`.`teknisi` AS `teknisi`,`antrian`.`tgl_selesai` AS `tgl_selesai` from ((`antrian` join `v_inventaris` on((`antrian`.`id_inventaris` = `v_inventaris`.`id_inventaris`))) left join `karyawan` on((`antrian`.`teknisi` = `karyawan`.`id_karyawan`)));

-- ----------------------------
-- View structure for `v_inventaris`
-- ----------------------------
DROP VIEW IF EXISTS `v_inventaris`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inventaris` AS select `inventaris`.`id_inventaris` AS `id_inventaris`,`inventaris`.`kode` AS `kode`,`inventaris`.`id_kategori` AS `inventaris_id_kategori`,`inventaris`.`id_divisi` AS `inventaris_id_divisi`,`inventaris`.`keterangan` AS `keterangan`,`inventaris`.`nama` AS `nama`,`inventaris`.`kondisi` AS `kondisi`,`inventaris`.`status` AS `status`,`divisi`.`nama` AS `divisi_nama`,`divisi`.`id_divisi` AS `id_divisi`,`kategori`.`id_kategori` AS `id_kategori`,`kategori`.`nama` AS `kategori_nama` from ((`inventaris` join `divisi` on((`inventaris`.`id_divisi` = `divisi`.`id_divisi`))) join `kategori` on((`kategori`.`id_kategori` = `inventaris`.`id_kategori`)));

-- ----------------------------
-- View structure for `v_user`
-- ----------------------------
DROP VIEW IF EXISTS `v_user`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user` AS select `user`.`id_user` AS `id_user`,`user`.`username` AS `username`,`user`.`password` AS `password`,`user`.`id_karyawan` AS `user_id_karyawan`,`karyawan`.`id_karyawan` AS `id_karyawan`,`karyawan`.`nama` AS `nama`,`karyawan`.`id_divisi` AS `karyawan_id_divisi`,`karyawan`.`jabatan` AS `jabatan`,`divisi`.`id_divisi` AS `id_divisi`,`divisi`.`nama` AS `divisi_nama` from ((`user` join `karyawan` on((`user`.`id_karyawan` = `karyawan`.`id_karyawan`))) join `divisi` on((`karyawan`.`id_divisi` = `divisi`.`id_divisi`)));
