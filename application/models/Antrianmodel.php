<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Antrianmodel extends CI_Model{
	
	public function __construct()
    {
       parent::__construct();
        $this->load->database();

    }
	
	public function insert($data){

		$this->db->insert('antrian', $data);
	}

	public function update($id, $data){
		$this->db->where('id_antrian', $id);
        $this->db->update('antrian', $data);   
	}

	public function delete($id){

	}

	public function listantrian(){

		$query = "select * from v_antrian where level_progress = 1";
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}

	public function listproses(){

		$query = "select * from v_antrian where level_progress = 2";
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}


	public function listselesai(){

		$query = "select * from v_antrian where level_progress = 3";
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}


	public function findbyid($id){
		$query = "select * from v_antrian where id = ".$id;
    	$rows = $this->db->query($query);
    	return $rows->row_array();
	}

	public function listantrianbydivisi($divisi){

		$query = "select * from v_antrian where id_divisi=".$divisi;
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}

	public function filter($awal, $akhir){

		$query = "select * from v_antrian where date(tgl) >= '".$awal."' or (date(tgl) >= '".$awal."' and (date(tgl_perbaikan) <= '".$akhir."' or date(tgl_selesai) <= '".$akhir."'))";
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}

	
}