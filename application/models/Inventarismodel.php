<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventarismodel extends CI_Model{
	
	public function __construct()
    {
       parent::__construct();
        $this->load->database();

    }
	
	public function insert($data){
		$this->db->insert('inventaris', $data);
	}

	public function update($id, $data){
		$this->db->where('id_inventaris', $id);
        $this->db->update('inventaris', $data); 
	}

	public function delete($id){

	}

	public function listall($divisi){

		$query = "select * from v_inventaris where id_divisi =".$divisi;
    	$rows = $this->db->query($query);
    	return $rows->result_array();
	}

	public function findbyid($id){
		$query = "select * from inventaris where id =".$id." limit 1";
    	$rows = $this->db->query($query);
    	return $rows->row_array();
	}

	public function findbykode($kode){

		$query = "select * from v_inventaris where kode ='".$kode."' limit 1";
    	$rows = $this->db->query($query);
    	return $rows->row_array();
	}


}