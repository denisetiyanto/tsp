<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
         <link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker.css" id="maincss">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-clearmin.min.css">
        

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/roboto.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/material-design.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/small-n-flat.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" id="bscss">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/summernote.css">


        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
        <!-- blueimp Gallery styles -->   


        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/selectize.css" />

        <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
                        <script src="<?php echo base_url();?>assets/js/moment.js"></script>

        <script type="text/javascript" src="<?php echo base_url();?>assets/js/selectize.min.js"></script>
        <style type="text/css">
        .datepicker {z-index: 1151 !important ;}
        </style>

        

        <title></title>
    </head>
    <body class="cm-no-transition cm-1-navbar">
        <div id="cm-menu">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="cm-flex"><a href="#"></a></div>
                <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
            </nav>
            <div id="cm-menu-content">
                <div id="cm-menu-items-wrapper">
                    <div id="cm-menu-scroller">
                        <ul class="cm-menu-items">

                       
                        <li class="cm-submenu">
                                <a class="sf-window-layout">Inventaris <span class="caret"></span></a>
                                <ul>
                                    <li><a href="<?php echo base_url();?>inventaris">List Inventaris</a></li>
                                    <li><a href="<?php echo base_url();?>inventaris/perbaikan">Perbaikan Inventaris</a></li>
                                </ul>
                            </li>
                           
                         <?php 
                      if($this->session->userdata('id_divisi') == "2")
                    { ?>   
                        <li class="cm-submenu">
                                <a class="sf-window-layout">Antrian <span class="caret"></span></a>
                                <ul>
                                    <li><a href="<?php echo base_url();?>antrian/create">Input Antrian</a></li>
                                    <li><a href="<?php echo base_url();?>antrian">List Antrian</a></li>
                                    <li><a href="<?php echo base_url();?>antrian/proses">List Proses</a></li>
                                    <li><a href="<?php echo base_url();?>antrian/histori">Histori</a></li>
                                </ul>
                            </li>
                    <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <header id="cm-header">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="btn btn-primary md-menu-white hidden-md hidden-lg" data-toggle="cm-menu"></div>
                <div class="cm-flex">
                    <h1>TSP</h1> 
                    
                </div> 
                <div class="dropdown pull-right">
                    <button class="btn btn-primary md-account-circle-white" data-toggle="dropdown" aria-expanded="false"></button>
                    <ul class="dropdown-menu">
                        <li class="disabled text-center">
                        </li>
                        <li class="divider"></li>
                       
                        <li>
                            <a href="<?php echo base_url();?>dashboard/logout"><i class="fa fa-fw fa-sign-out"></i> Sign out</a>
                        </li>
                    </ul>
                </div>              
               
            </nav>
        </header>
        <div id="global">
            <div class="container-fluid">
              

               <?php echo $content;?>

            </div>

                      

            <footer class="cm-footer"><span class="pull-left">Connected as <?php echo $this->session->userdata('username');?> Divisi <?php echo $this->session->userdata('divisi_nama');?></span><span class="pull-right">&copy; TSP</span></footer>
        </div>
        <script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.cookie.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/fastclick.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/clearmin.min.js"></script>


        <script src="<?php echo base_url();?>assets/js/demo/home.js"></script>       
        <script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>
       
        <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>

    </body>
    
</html>