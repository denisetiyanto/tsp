

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Proses</div>
                            <div class="panel-body">

                            
                                 
                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Kode Inventaris</th>
                                            <th>Jenis</th>           
                                            <th>Tanggal Proses</th>
                                            <th>Teknisi</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
        <div id="selesaimodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                    
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            Pekerjaan selesai
                            <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
                        </h4>
                    </div>
                      <form role="form" id="formag" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>antrian/update_selesai">
                      <input id="id_antrian" type="hidden" name="id_antrian" class="form-control">
                      <input id="id_inventaris" type="hidden" name="id_inventaris" class="form-control">
                    <div class="modal-body"> 
                        <h5>Anda yakin pekerjaan sudah selesai ?</h5>
                    
                     </div>   
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-primary">Selesai</button>
                        
                                               
                        </div>   
                        </form>                  
                        
                </div>  
            </div>  
           </div>            
                 
     <script type="text/javascript">
     	
	$(document).ready(function() {


    var t = $('#example1').DataTable({    
    	"deferRender": true,	
        "ajax": '<?php echo base_url();?>antrian/get_all_antrian/2',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_antrian",
             "visible" : false 
         	},
            { "data": "kode" },  
            { "data": "nama"},           
            { "data": "tgl_perbaikan"},
            { "data": "karyawan_nama"},
            { "data": "id_antrian",
                "render": function ( data, type, full, meta ) {
                    return '<button type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#selesaimodal" type="button" class="btn btn-danger">Selesai</button>';
                }
            },
                   
            
            
        ]
    } );

    $('#example1 tbody').on( 'click', 'button', function () {
        var data = t.row($(this).parents('tr')).data();
        console.log( data );

        $('#id_antrian').val(data.id_antrian);
        $('#id_inventaris').val(data.id_inventaris);
    } );

    

   /* t.on('click', 'tr', function () {
        var data = t.row( this ).data();

        $('#id').val(data.id);
        $('#nama').val(data.nama);
        $('#alamat').val(data.alamat); 
        $('#telepon').val(data.telepon);       
        $('#kode').val(data.kode);
        $('#status').val(data.status);
        $('#simag').css('display', 'none');
        $('#edag').css('display', 'inline');
        $('#delag').css('display', 'inline');
        $('#delete').attr('href','<?php echo base_url();?>agen/remove/'+data.id);
        $('#formag').attr('action', '<?php echo base_url();?>agen/update');
        $('#tambah').click();

    });*/

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

  /*  $('#myModal').on('hidden.bs.modal', function (e) {

    	$('#nama').val("");
        $('#alamat').val("");  
        $('#telepon').val("");      
        $('#kode').val("");
        $('#id').val("");

   		$('#simag').css('display', 'inline');
        $('#edag').css('display', 'none');
        $('#delag').css('display', 'none');
        $('#formag').attr('action', '<?php echo base_url();?>agen/save');
	});

    $("#delag").click(function(e) {
    e.preventDefault();    

    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function() {

             var targetUrl = $(this).attr("href"); 
            window.location.href = targetUrl;
        });
  });  */
    
} );
</script>