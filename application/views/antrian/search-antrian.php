

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Search</div>
                            <div class="panel-body">

                      
                                 <div class="row">

                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Kode Inventaris</th>
                                            <th>Jenis</th>    
                                            <th>Keluhan</th>       
                                            <th>Tgl Masuk</th> 
                                            <th>Tgl Proses</th>                                            
                                            <th>Teknisi</th>
                                            <th>Tgl Selesai</th> 
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table> 
                                </div>   
                            </div>
                        </div>
                    </div>

                    </div>
         <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>

     <script type="text/javascript">
     	
	$(document).ready(function() {
$('.input-daterange input').each(function() {
      $(this).datepicker({format: 'yyyy-mm-dd'});
  });
    var t = $('#example1').DataTable({    
    	"deferRender": true,	
        "ajax": '<?php echo base_url();?>antrian/search/'+<?php echo $awal;?>+'/<?php echo $akhir;?>',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_inventaris",
             "visible" : false 
         	},
            { "data": "kode" },  
            { "data": "nama"}, 
            { "data": "keluhan"}, 
            { "data": "tgl"},         
            { "data": "tgl_perbaikan"},
            { "data": "karyawan_nama"},
            { "data": "tgl_selesai"}
                   
            
            
        ]
    } );



  

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();



 
    
} );


</script>