

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Histori</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div style="margin-bottom:20px;">
                        <form class="form-inline">

                      <div class="form-group">
                    <label for="kategori" class="col-sm-2 control-label">Search by period</label>

                        <div class="input-group input-daterange">
                         <input id="awal" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd">
                         <span class="input-group-addon"> - </span>
                         <input id="akhir" type="text" class="form-control" placeholder="yyyy-mm-dd">
                       </div>
                    
                     <button type="button" class="btn btn-primary" id="cari">Cari</button>

                     </div>


                   </form>
                   <hr>
                                 </div> 
                                </div>
                                 <div class="row">

                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Kode Inventaris</th>
                                            <th>Jenis</th>    
                                            <th>Keluhan</th>       
                                            <th>Tgl Masuk</th> 
                                            <th>Tgl Proses</th>                                            
                                            <th>Teknisi</th>
                                            <th>Tgl Selesai</th> 
                                            <th>Progress</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table> 
                                </div>   
                            </div>
                        </div>
                    </div>

                    </div>
  

     <script type="text/javascript">
     	
	$(document).ready(function() {

    
      $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
  


    
init();

$('#cari').click(function(){
    var url, awal = $('#awal').val(), akhir = $('#akhir').val();
    

      if($('#awal').val() == "" || $('#akhir').val() == ''){
        awal = 0;
        akhir = 0;
      }
      url = "<?php echo base_url();?>antrian/search/"+awal+"/"+akhir;
    

    RefreshTable('#example1', url);

  });
 
    
} );

function RefreshTable(tableId, urlData)
    {


      $.ajax({
      url: urlData,
      type: "GET",
      dataType: "json",
    }).done(function(x){

      table = $(tableId).dataTable();
      oSettings = table.fnSettings();
      table.fnClearTable(this);

      $.each(x, function(i, item){
          table.oApi._fnAddData(oSettings, item);
      });

     oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
      table.fnDraw();
    });
    }

function init(){
    var t = $('#example1').DataTable({    
        "deferRender": true,    
        "ajax": '<?php echo base_url();?>antrian/get_all_antrian/3',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_inventaris",
             "visible" : false 
            },
            { "data": "kode" },  
            { "data": "nama"}, 
            { "data": "keluhan"}, 
            { "data": "tgl"},         
            { "data": "tgl_perbaikan"},
            { "data": "karyawan_nama"},
            { "data": "tgl_selesai"},
            { "data": "level_progress",
                "render": function ( data, type, full, meta ) {
                    var status;
                    if(data == "1"){
                    status = '<span>Menunggu Antrian</span>';
                    }else if(data == "2"){
                    status = '<span>Proses Perbaikan</span>';
                    }else if(data == "3"){
                    status = '<span>Selesai</span>';
                    }

                    return status;
                }

             }
                   
            
            
        ]
    } );

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}
</script>