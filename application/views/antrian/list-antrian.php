

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Antrian</div>
                            <div class="panel-body">

                            
                                 
                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Kode Inventaris</th>
                                            <th>Jenis</th>
                                            <th>Divisi</th>           
                                            <th>Tanggal Masuk</th>
                                            <th>Keluhan</th>
                                            <th>&nbsp;</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>


          <div id="teknisimodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                    
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            Teknisi
                            <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
                        </h4>
                    </div>
                      <form role="form" id="formag" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>antrian/update_perbaikan">
                      <input id="id_antrian" type="hidden" name="id_antrian" class="form-control">

                    <div class="modal-body"> 

                    <div class="form-group">
                            <label for="kategori" class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-9">

                                <select class="demo-default" name="teknisi" id="teknisi">
                                    <?php foreach ($teknisi as $key) { ?>

                                    <option value="<?php echo $key['id_karyawan'];?>"><?php echo $key['nama'];?></option>
                                    
                                    <?php } ?>
                                    
                                </select>

                                <script>
                                    $('#teknisi').selectize({
                                       
                                        sortField: {
                                            field: 'text',
                                            direction: 'asc'
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                     </div>   
                        <div class="modal-footer">
                           <button id="teknisibtn" type="submit" class="btn btn-primary">Proses</button>
                        
                                               
                        </div>   
                        </form>                  
                        
                </div>  
            </div>  
           </div>      
     <script type="text/javascript">

          	
	$(document).ready(function() {


    var t = $('#example1').DataTable({    
    	"deferRender": true,	
        "ajax": '<?php echo base_url();?>antrian/get_all_antrian/1',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_antrian",
             "visible" : false 
         	},
            { "data": "kode" },  
            { "data": "nama"}, 
            { "data": "divisi_nama"},          
            { "data": "tgl"},
            { "data": "keluhan"},
            { "data": "id_inventaris",
                "render": function ( data, type, full, meta ) {
                    return '<button id="perbaiki" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#teknisimodal" type="button" class="btn btn-danger">Perbaiki</button>';
                }
            },
                   
            
        ]
    } );

     $('#example1 tbody').on( 'click', 'button', function () {
        var data = t.row($(this).parents('tr')).data();
        console.log( data );

        $('#id_antrian').val(data.id_antrian);
    } );

   

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

 
    
} );
</script>