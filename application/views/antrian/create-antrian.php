<div class="row cm-fix-height">
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">Input Antrian</div>
                            <div class="panel-body" style="min-height: 214px;">
                                <form class="form-inline">
                                    
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-4 control-label">Kode Barang</label>
                                        <div class="col-sm-8">
                                            <input id="kodecari" type="text" name="kodecari" class="form-control">
                                         </div>
                                    </div>
                                    
                                                                       
                                    <div class="form-group">
                                        <button type="button" id="cari" class="btn btn-danger">Cari</button>
                                    </div>
                                </form>  
                                <hr>

                                <div style="display:none" id="alert" class="alert alert-info alert-dismissible fade in shadowed" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                Inventaris tidak ditemukan.
                                </div>
                              <form style="display:none" id="formantrian" class="form-horizontal" method="post" action="<?php echo base_url();?>antrian/save">
                                    <input id="id_inventaris" type="hidden" name="id_inventaris" class="form-control">
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Kode Barang</label>
                                        <div class="col-sm-10">
                                            <input readonly="true" id="kode" type="text" name="kode" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Jenis</label>
                                        <div class="col-sm-10">
                                            <input readonly="true"  id="jenis" type="text" name="jenis" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Divisi</label>
                                        <div class="col-sm-10">
                                            <input readonly="true"  id="divisi" type="text" name="divisi" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Keluhan</label>
                                        <div class="col-sm-10" >
                                           <textarea id="keluhan" name="keluhan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                                                       
                                    <div class="form-group" style="margin-bottom:0">
                                        <div class="col-sm-offset-2 col-sm-10 text-right">
                                            <button type="reset" id="cankat" class="btn btn-default">Cancel</button>
                                            <button type="submit" id="simkat" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </form>                                    
                        </div>
                </div>
            </div>
        </div>

 <script type="text/javascript">
$(document).ready(function() {

    $('#cari').click(function(){

        $.ajax({
                type: 'POST',
                url: "<?php echo base_url()?>antrian/get_antrian/"+ $('#kodecari').val(),
                success: function(data) {

                    if(data.data != null){

                        $('#id_inventaris').val(data.data.id_inventaris);
                        $('#jenis').val(data.data.nama);
                        $('#kode').val(data.data.kode);
                        $('#divisi').val(data.data.divisi_nama);


                        $('#formantrian').css('display', 'block');
                        $('#alert').css('display', 'none');
                    }else{
                        $('#alert').css('display', 'block');
                        $('#formantrian').css('display', 'none');

                    }
                   

                }
            });
    })
  });

</script>