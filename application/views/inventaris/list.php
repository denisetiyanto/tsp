

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Inventaris</div>
                            <div class="panel-body">

                             
                            <div class="row">     
                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Kode</th>                                            
                                           
                                            <th>Kategori</th>
                                            <th>Keterangan</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table> 
                                </div>   
                            </div>
                        </div>
                    </div>
        <div id="laporkanmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                    
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            Keluhan
                            <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
                        </h4>
                    </div>
                    <form id="formantrian" class="form-horizontal" method="post" action="<?php echo base_url();?>antrian/save">
 
                    <div class="modal-body"> 

                                    <input id="id_inventaris" type="hidden" name="id_inventaris" class="form-control">
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Kode Barang</label>
                                        <div class="col-sm-10">
                                            <input readonly="true" id="kode" type="text" name="kode" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Jenis</label>
                                        <div class="col-sm-10">
                                            <input readonly="true"  id="jenis" type="text" name="jenis" class="form-control">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-2 control-label">Keluhan</label>
                                        <div class="col-sm-10" >
                                           <textarea id="keluhan" name="keluhan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                                                       
                                  
                                 
                     </div>   
                        <div class="modal-footer">
                           <button id="laporkanbtn" type="submit" class="btn btn-primary">Laporkan</button>
                        
                                               
                        </div>   
                        </form>                  
                        
                </div>  
            </div>  
           </div>             
                 
     <script type="text/javascript">
     	
	$(document).ready(function() {


    var t = $('#example1').DataTable({    
    	"deferRender": true,	
        "ajax": '<?php echo base_url();?>inventaris/get_all_data/<?php echo $this->session->userdata("id_divisi");?>',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_inventaris",
             "visible" : false 
         	},
            { "data": "kode" },             
            { "data": "kategori_nama"},
            { "data": "keterangan"},
            { "data": "status",
                "render": function ( data, type, full, meta ) {
                    var status;
                    if(data == "1"){
                    status = '<span>Sedang perbaikan</span>';
                    }else if(data == "0"){
                    status =  '<button id="laporkan" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#laporkanmodal" type="button" class="btn btn-danger">Laporkan</button>';
                    }

                    return status;
                }
            },
                   
            
            
        ]
    } );

    $('#example1 tbody').on( 'click', 'button', function () {
        var data = t.row($(this).parents('tr')).data();
        console.log( data );

        $('#id_inventaris').val(data.id_inventaris);
        $('#kode').val(data.kode);
        $('#jenis').val(data.nama);

    } );

   

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

  
    
} );
</script>