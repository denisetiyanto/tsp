

                <div class="row cm-fix-height">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Perbaikan Inventaris</div>
                            <div class="panel-body">

                            
                                 
                                 <table id="example1" class="table table-hover" style="margin-bottom:0">
                                  
                                    <thead>
                                        <tr>
                                           <th>#</th>
                                            <th>ID</th>
                                            <th>Kode Inventaris</th>
                                            <th>Jenis</th>                                                 
                                            <th>Tanggal Masuk</th>
                                            <th>Keluhan</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                           
                                       
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>

                    <div id="detailmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                    
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            Detail
                            <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
                        </h4>
                    </div>

                    <div class="modal-body"> 

                        <div class="row">
                            <div class="col-lg-6">
                                <div id="kodedetail"></div>
                            </div>
                            <div class="col-lg-6">
                                <div id="jenisdetail"></div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="progress">
                              <div class="progress-bar progress-bar-danger" style="width: 33.33%">
                                <span class="show">Menunggu antrian</span>
                              </div>
                              <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 0%">
                               
                              </div>
                              <div class="progress-bar progress-bar-success" style="width: 0%">
                                
                              </div>
                            </div>
                        </div>

                        <div class="row">
                        <div class="col-lg-4">
                            <table class="table">
                            <tbody>
                              <tr>
                               <th>Tanggal Masuk</th>                         
                                
                             </tr> 
                             <tr>
                               <td class="tglmasuk"></td>  
                             </tr>  
                             <tr>
                               <th>Keluhan</th>                                                      
                             </tr> 
                             <tr>
                               <td class="keluhan"></td>                                                      
                             </tr>                                                   
                                           
                            </tbody>
                            </table>
                        </div>  
                        <div class="col-lg-4">  
                            <table class="table">
                            <tbody>
                              <tr>
                               <th>Tanggal Perbaikan</th>                         
                                
                             </tr> 
                             <tr>
                               <td class="tglperbaikan"> - </td>   
                             </tr>  
                             <tr>
                               <th>Teknisi</th>                                                      
                             </tr> 
                             <tr>
                               <td class="teknisi"> - </td>                                                       
                             </tr>                                               
                                           
                            </tbody>
                            </table>
                        </div>  
                        <div class="col-lg-4">
                            <table class="table">
                            <tbody>
                              <tr>
                               <th>Tanggal Selesai</th>                         
                                
                             </tr> 
                             <tr>
                               <td class="tglselesai"> - </td>   
                             </tr>  
                                 
                                                                            
                                           
                            </tbody>
                            </table>
                        </div>
                     </div> 
                     </div>   
                          
                             
                        
                </div>  
            </div>  
           </div> 

                 
     <script type="text/javascript">
     	
	$(document).ready(function() {


    var t = $('#example1').DataTable({    
    	"deferRender": true,	
        "ajax": '<?php echo base_url();?>inventaris/get_data_antrian/<?php echo $this->session->userdata("id_divisi");?>',
        "columns": [
            { "data" : null,
              "orderable": false,
              "searchable": false
            },
            { "data": "id_antrian",
             "visible" : false 
            },
            { "data": "kode" },  
            { "data": "nama"},       
            { "data": "tgl"},
            { "data": "keluhan"},
            { "data": "id_antrian",
                "render": function ( data, type, full, meta ) {
                    return '<button id="detailbtn" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#detailmodal" type="button" class="btn btn-danger">Detail</button>';
                }
            },
            
                   
            
        ]
    } );

    $('#example1 tbody').on( 'click', 'button', function () {
        var data = t.row($(this).parents('tr')).data();
        console.log( data );
        $('#kodedetail').html('');
        $('#jenisdetail').html('');
        $('.progress-bar-warning').css('width', '0%');
        $(".progress-bar-warning span").html('');

        $('.progress-bar-success').css('width', '0%');
        $(".progress-bar-success span").html('');


        $('#kodedetail').append('<h5>Kode : <strong>'+data.kode+'</strong></h5>');
        $('#jenisdetail').append('<h5>Jenis : <strong>'+data.nama+'</strong></h5>');
        $('.tglmasuk').text(data.tgl);
        $('.keluhan').text(data.keluhan);

        $('.tglperbaikan').text(data.tgl_perbaikan);
        $('.teknisi').text(data.karyawan_nama);

        $('.tglselesai').text(data.tgl_selesai);

        if(data.level_progress == 2){
            $('.progress-bar-warning').css('width', '33.33%');
            $(".progress-bar-warning").append('<span class="show">Proses perbaikan</span>');
        }else if(data.level_progress == 3){

            $('.progress-bar-warning').css('width', '33.33%');
            $(".progress-bar-warning").append('<span class="show">Proses perbaikan</span>');

            $('.progress-bar-success').css('width', '33.33%');
            $(".progress-bar-success").append('<span class="show">Selesai</span>');
        }

        
        
    } );

   /* t.on('click', 'tr', function () {
        var data = t.row( this ).data();

        $('#id').val(data.id);
        $('#nama').val(data.nama);
        $('#alamat').val(data.alamat); 
        $('#telepon').val(data.telepon);       
        $('#kode').val(data.kode);
        $('#status').val(data.status);
        $('#simag').css('display', 'none');
        $('#edag').css('display', 'inline');
        $('#delag').css('display', 'inline');
        $('#delete').attr('href','<?php echo base_url();?>agen/remove/'+data.id);
        $('#formag').attr('action', '<?php echo base_url();?>agen/update');
        $('#tambah').click();

    });*/

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

  /*  $('#myModal').on('hidden.bs.modal', function (e) {

    	$('#nama').val("");
        $('#alamat').val("");  
        $('#telepon').val("");      
        $('#kode').val("");
        $('#id').val("");

   		$('#simag').css('display', 'inline');
        $('#edag').css('display', 'none');
        $('#delag').css('display', 'none');
        $('#formag').attr('action', '<?php echo base_url();?>agen/save');
	});

    $("#delag").click(function(e) {
    e.preventDefault();    

    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function() {

             var targetUrl = $(this).attr("href"); 
            window.location.href = targetUrl;
        });
  });  */
    
} );
</script>