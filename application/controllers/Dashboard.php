<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {


  public function __construct()
  {
      parent::__construct();

      $this->load->library('session');
       $this->load->helper('url');
       $this->load->helper('form');
      $this->load->model('Authmodel');
  
    }

  public function index(){

      if($this->session->userdata('logged_in'))
         {
        // $session_data = $this->session->userdata('logged_in');
       //  $data['nama'] = $session_data['nama'];
       //  $data['peran'] = $session_data['peran'];

          
         $content['content'] =  $this->load->view('inventaris/list', '', true);
        $this->load->view('index',$content);
         }
         else
         {

           //redirect('login', 'refresh');
          $this->load->view('login');
         }


  }

  public function home(){

          $content['content'] =  $this->load->view('inventaris/list', '', true);
        $this->load->view('index',$content);
  }

  public function login(){

      $username = $this->input->post('username');
      $password= $this->input->post('password');
      $result = $this->Authmodel->auth($username, $password);

     if($result)
     {
       $sess_array = array();
       foreach($result as $row)
       {
         $sess_array = array(
           'id' => $row->id_user,
           'username' => $row->username,
           'nama' => $row->nama,  
           'id_divisi' => $row->id_divisi,
           'divisi_nama' => $row->divisi_nama,         
           'logged_in' => TRUE
         );
         $this->session->set_userdata($sess_array);
       }
      // return TRUE;

     // $data['row'] = "";
     // $content['content'] = $this->load->view('home', $data, true);
    //  $this->load->view('index', $content);

        redirect('dashboard', 'refresh');

  //  echo $this->session->userdata('cabang_id');
     }
     else
     {
     //  $this->form_validation->set_message('check_database', 'Invalid username or password');
      // return false;
      //$this->load->view('login');

      redirect('dashboard', 'refresh');

     }
  }

public function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('dashboard', 'refresh');
 }


}
