<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller {

	public function __construct()
    {
       parent::__construct();
       $this->load->helper('url');
       $this->load->helper('form');
       
       $this->load->model('Antrianmodel');
       $this->load->model('Inventarismodel');
       $this->load->model('Karyawanmodel');
       $this->load->library('session');
		if(!$this->session->userdata('logged_in'))
        {
            redirect('dashboard');
        }
    }
	
	public function index()
	{
		$data['teknisi'] = $this->Karyawanmodel->listallteknisi();
		$content['content'] =  $this->load->view('antrian/list-antrian', $data, true);
        $this->load->view('index',$content);
	}

	public function proses()
	{
		$content['content'] =  $this->load->view('antrian/list-proses', '', true);
        $this->load->view('index',$content);
	}

	public function histori()
	{
		$content['content'] =  $this->load->view('antrian/list-selesai', '', true);
        $this->load->view('index',$content);
	}

	public function create(){
		$content['content'] =  $this->load->view('antrian/create-antrian', '', true);
        $this->load->view('index',$content);
	}

	public function save(){

		$data['id_inventaris'] = $this->input->post('id_inventaris');
		$data['keluhan'] = $this->input->post('keluhan');
		
		$this->Antrianmodel->insert($data);

		$data1['status'] = 1;
		$this->Inventarismodel->update($data['id_inventaris'], $data1);	


	}

	public function update(){
		
		$id = $this->input->post('id');
		$data['id_inventaris'] = $this->input->post('id_inventaris');


		$this->Antrianmodel->update($id, $data);	
	}

	public function update_perbaikan(){
		
		$id = $this->input->post('id_antrian');
		$data['level_progress'] = 2;
		$data['teknisi'] = $this->input->post('teknisi');
		$data['tgl_perbaikan'] = date('Y-m-d H:i:s');


		$this->Antrianmodel->update($id, $data);	
	}

	public function update_selesai(){
		
		$data['id_inventaris'] = $this->input->post('id_inventaris');
		$id = $this->input->post('id_antrian');
		$data['level_progress'] = 3;		
		$data['tgl_selesai'] = date('Y-m-d H:i:s');


		$this->Antrianmodel->update($id, $data);	

		$data1['status'] = 0;
		$this->Inventarismodel->update($data['id_inventaris'], $data1);	
	}

	public function get_all_antrian($progress=0){
		
		if($progress == 1){
			$data = $this->Antrianmodel->listantrian();
		}else if($progress == 2){
			$data = $this->Antrianmodel->listproses();
		}else if($progress == 3){
			$data = $this->Antrianmodel->listselesai();
		}
		

	   header('Content-Type: application/json');
       echo json_encode(array("data" => $data));
	}



	public function get_antrian($kode=''){
		
		$data = $this->Inventarismodel->findbykode($kode);

	   header('Content-Type: application/json');
       echo json_encode(array("data" => $data));
	}

	public function search($awal='',$akhir=''){
		
		$data = $this->Antrianmodel->filter($awal, $akhir);

		header('Content-Type: application/json');
	    echo json_encode($data);
	}

	public function filter(){
		
		$data['awal'] = $this->input->post('awal');
		$data['akhir'] = $this->input->post('akhir');
		
	  	$content['content'] =  $this->load->view('antrian/search-antrian', $data, true);
        $this->load->view('index',$content);
	}
}
