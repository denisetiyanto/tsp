<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends CI_Controller {

	public function __construct()
    {
       parent::__construct();
       $this->load->helper('url');
       $this->load->helper('form');
       
       $this->load->model('Divisimodel');
       $this->load->library('session');
       if(!$this->session->userdata('logged_in'))
        {
            redirect('dashboard');
        }

    }

	public function index()
	{
		$data['rows'] = $this->Divisimodel->listall();
        $content['content'] =  $this->load->view('divisi/list',$data, true);
        $this->load->view('index',$content);
	}

	public function save(){
		$data['nama'] = $this->input->post('nama');

		$this->Divisimodel->insert($data);	
	}

	public function update(){

		$id = $this->input->post('id');
		$data['nama'] = $this->input->post('nama');

		$this->Divisimodel->update($id, $data);	

	}

	public function get_data_all(){
		

	}
}
