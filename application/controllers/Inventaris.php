<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaris extends CI_Controller {

	public function __construct()
    {
       parent::__construct();
       $this->load->helper('url');
       $this->load->helper('form');
       
       $this->load->model('Divisimodel');
       $this->load->model('Inventarismodel');
       $this->load->model('Antrianmodel');
       $this->load->library('session');
       if(!$this->session->userdata('logged_in'))
        {
            redirect('dashboard');
        }

    }

	public function index()
	{
		
        $content['content'] =  $this->load->view('inventaris/list', '', true);
        $this->load->view('index',$content);
	}

	public function perbaikan()
	{
		
        $content['content'] =  $this->load->view('inventaris/list-perbaikan', '', true);
        $this->load->view('index',$content);
	}

	public function save(){
		$data['kode'] = $this->input->post('kode');
		$data['nama'] = $this->input->post('nama');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_kategori'] = $this->input->post('id_kategori');
		$data['id_divisi'] = $this->input->post('id_divisi');


		$this->Divisimodel->insert($data);	
	}

	public function update(){

		$id = $this->input->post('id_inventaris');
		$data['kode'] = $this->input->post('kode');
		$data['nama'] = $this->input->post('nama');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_kategori'] = $this->input->post('id_kategori');
		$data['id_divisi'] = $this->input->post('id_divisi');

		$this->Inventarismodel->update($id, $data);	
	}

	

	public function get_all_data($divisi=0){
			

	
	   $data = $this->Inventarismodel->listall($divisi);
	

	   header('Content-Type: application/json');
       echo json_encode(array("data" => $data));

	}

	public function get_data_antrian($divisi=0){
			

	
		$data = $this->Antrianmodel->listantrianbydivisi($divisi);
	

	   header('Content-Type: application/json');
       echo json_encode(array("data" => $data));

	}
}
